# Pylint

Pylint-GitLab integration demo repository. Checkout the [accompanying Bits & Bytes article](https://docs.mpcdf.mpg.de/bnb/213.html).

See [GitLab CI configuration](https://gitlab.mpcdf.mpg.de/mpcdf/training/pylint/-/blob/main/.gitlab-ci.yml) for integration details. Changes in the Pylint output can be inspected in each [merge request](https://gitlab.mpcdf.mpg.de/mpcdf/training/pylint/-/merge_requests/2). 

